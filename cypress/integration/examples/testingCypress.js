describe('Probando cypress', function () {
    it('busqueda de informacion en reddit', function () {
        cy.visit('https://example.cypress.io')

        cy.contains('type').click()
        cy.go('back')
        cy.contains('Querying').click({ force: true })
        cy.reload()
        //cy.contains('GitHub').click()
    })
    it('Scrolling , esto puede aplicar para paginas' +
        ' renderizadas en tiempo real(segun vas scrolleando)',
        function () {
            cy.contains('cy.get()').click()
            cy.scrollTo(0, 3500)
            cy.scrollTo(3500, 0)
            cy.get('#sidebar').scrollTo('bottom')
        })
})